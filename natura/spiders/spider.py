# import sys
# reload(sys)
# sys.setdefaultencoding('utf8')

import scrapy
from scrapy.spider import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.exceptions import CloseSpider
from natura.items import NaturaItem

class NaturaSpider(CrawlSpider):
	name = 'natura'
	item_count = 0
	allowed_domain = ['www.natura.cl']
	start_urls = ['http://natura.cl/c/nuestros-productos/']

	rules = {
		# Para cada item
		Rule(LinkExtractor(allow =(), restrict_xpaths = ('//div[@class="pagination_cat"]/span[text()="2"]'))),
		Rule(LinkExtractor(allow =(), restrict_xpaths = ('//div[@class="title_prod floatleft"]/h2/a')),
							callback = 'parse_item', follow = False)
	}

	def parse_item(self, response):
		ml_item = NaturaItem()
		#info de producto
		ml_item['titulo'] = response.xpath('//h2[@ng-bind-html="product.displayName | trust"]/text()').extract()
		#imagenes del producto
		ml_item['image_urls'] = response.xpath('//img[@class="zoomImg"]/@src').extract()
		ml_item['image_name'] = response.xpath('//h2[@ng-bind-html="product.displayName | trust"]/text()').extract()
		self.item_count += 1
		if self.item_count > 5:
			raise CloseSpider('item_exceeded')
		yield ml_item
